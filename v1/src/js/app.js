// Dein App-Code hier

$(document).ready(function() {

    //Als erstes schreiben wir einen leeren string in den div
    $("#app").html("");

    //Jetzt wird mit einer schleife der array ausgelesen und die werte task und description in den div geschrieben
    //dabei trenne ich den reinen content mit den aktionsbuttons

    // Variante 1
    for(var i = 0; i < ToDoList.length; i++) {
        // Variablen Definieren
        important = (ToDoList[i].important);
        done = (ToDoList[i].done);
        task = (ToDoList[i].task);
        description = (ToDoList[i].description);

        // Hier Definiere ich die Schleife
        todos = (
            "<div class='todoBox'>" +
                "<div class='todoAktion'>" +
                    "<div class='todoImportant'>" + important + "</div>" +
                    "<div class='todoDone'>" + done + "</div>" +
                    "<div class='todoClose'></div>" +
                "</div>" +
                "<h1 class='todoListHeadline'>" + task + "</h1>" +
                "<hr>" +
                "<article class='todoTaskDescription'>" + description + "</article>" +
            "</div>"
        );

        $("#app").append(todos);

    }

    taskDone = (
        "<h1 class='category pointer excluded'>Erledigte Aufgaben</h1>" +
        "<div id='taskDone' class='excluded'></div>"
    );

    taskFinished = (
    "<h1 class='category pointer excluded'>Gelöschte Aufgaben</h1>" +
    "<div id='taskFinished' class='excluded dn'></div>"
    );

    $(document).ready(function(){
        $(".todoImportant:contains('true')")
            .closest(".todoBox")
            .addClass("importantChecked")
            .prependTo("#app")
        ;

        $(".todoDone:contains('true')")
            .closest(".todoBox")
            .addClass("doneChecked")
            .appendTo("#taskDone")
        ;

        $(function () {
            $("#app").sortable({
                cursor: 'move',
                opacity: 0.8,
                placeholder: "todobox-placeholder",
                start: function (event, ui) {
                    ui.placeholder.height(ui.item.height());
                    ui.placeholder.width(ui.item.width());
                },
                cancel: function(event, ui) {
                    $("h1.category").disableSelection();
                },
                over: function(event, ui){
                    if($(ui.item.hasClass("excluded"))){
                        $(ui.sender).sortable("disable");
                    }
                }

                /*
                stop: function (e,ui) {
                    if($(ui.item.hasClass("excluded"))){
                        $(ui.sender).sortable("disable");
                    }
                },
                over:function(e,ui){
                 stop = true;
                }*/
            });
        });
    });


    $("#app").append(taskDone + taskFinished);

});
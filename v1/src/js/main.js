// Deine Initialisierung hier

$(document).ready(function() {
    $(".todoImportant").click(function() {
        $(this)
            .closest(".todoBox")
            .addClass("importantChecked")
            .removeClass("doneChecked closeChecked")
            .prependTo("#app")
        ;
    });

    $(".todoDone").click(function() {
        $(this)
            .closest(".todoBox")
            .addClass("doneChecked")
            .removeClass("importantChecked closeChecked")
            .prependTo("#taskDone")
        ;
    });

    $(".todoClose").click(function() {
        $(this)
            .closest(".todoBox")
            .addClass("closeChecked")
            .removeClass("importantChecked doneChecked")
            .prependTo("#taskFinished")
        ;
    });

    $("h1.todoListHeadline").click(function(){
        $(this)
            .next()
            .next()
            .slideToggle()
        ;
    });

    $("h1.category").click(function(){
        $(this)
            .next()
            .slideToggle()
        ;
    });

    // den Namen in den footer schreiben
    $("footer").html("2015 &copy; — <span>Özkan Kaygisiz</span>");
});
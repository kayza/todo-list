module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        compress: {
            main: {
                options: {
                    archive: 'abgabe.zip'
                },
                files: [
                    {src: ['src/**'], dest: './', filter: 'isFile'}, // includes files in path and its subdirs
                ]
            }
        },

        bower: {
            options: {
                layout: 'byType',
                install: true,
                verbose: false,
                cleanTargetDir: false,
                cleanBowerDir: false
            },
            src: {
                options: {
                    targetDir: 'src/lib',
                    bowerOptions: {
                        production: false
                    }
                }
            },
            dest: {
                options: {
                    targetDir: 'dest/lib',
                    bowerOptions: {
                        production: true
                    }
                }
            }
        },

        jshint: {
            all: ['Gruntfile.js', 'src/**/*.js'],
            src: ['src/**/*.js']
        }
    });

    // Default task(s).
    grunt.registerTask('default', []);

};

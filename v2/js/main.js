(function($) {
    $('document').ready(function () {
        initApp();
        sendRequest();
        modals();
        createTask();
        editTask();
        deleteTask();
        deleteTaskFinal();
    });
})(jQuery);


var toDoList = jQuery('#todo-list');
var deletedList = jQuery('#deleted-tasks');

function initApp() {

    // if js active and/but no tasks in the queue
    if($('#todo-list').html().length < 0) {
        $('.ifNoJs').remove();
    } else {
        $('.ifNoJs').remove();
        noTasksinQueue();
    }

    // if no js
    $('#deleteTaskHeadline').removeClass('hide');
    $('.ifDeletedTask').removeClass('hide');

    // Name in footer
    $('footer').html('<p>&copy; kayzamedia 2016</p>');
}

function toggleAttr(el, attribute, vals){

    if ($(el).attr(attribute) ==  vals[0]){
        $(el).attr(attribute, vals[1]);

    } else if ($(el).attr(attribute) == vals[1]) {
        $(el).attr(attribute, vals[0]);
    }
}

function makeClickable(elem) {
    var that = elem.find('.panel-heading');

    that.on('click', function()
    {
        $('.panel-body').each(function () {
            if ($(this).closest('.panel').find('.panel-heading')[0] != that[0]) {
                $(this).slideUp('slow');
            }
        });

        elem
            .find('.panel-body')
            .slideDown('slow')
        ;
    });
}



/*******
 * Set Task important or unimportant
 *******/
function sendRequest() {
    $.get('src/php/todolist.php', function (data) {
        for(var iteration = 0; iteration < data.length; iteration++) {
            var description = data[iteration].description.replace(/(\\n)+/g, '<br>');
            var taskHeadline = data[iteration].task;
            var taskID = data[iteration].id;
            var statImportant = data[iteration].important;
            var statDeleted = data[iteration].deleteTask;
            var statDone = data[iteration].done;
            var panelColor = null;

            if(statImportant == 1) {
                panelColor = 'danger';
            } else if(statDone == 1) {
                panelColor = 'info';
            } else if(statDeleted == 1) {
                panelColor = 'success';
            } else {
                panelColor = 'default';
            }

            var html = $(
                '<div class="panel panel-'+ panelColor +' task">' +
                '   <div class="panel-heading">' +
                '       <h3 class="panel-title">' + taskHeadline + '</h3>' +
                '       <div id="' + taskID + '" class="icons pull-right">' +
                '           <i class="glyphicon glyphicon-warning-sign" data-toggle="tooltip" data-placement="top" title="Als Wichtig markieren" data-status="' + statImportant + '"></i>' +
                '           <i class="glyphicon glyphicon-ok-circle" data-toggle="tooltip" data-placement="top" title="Als Erledigt markieren" data-status="' + statDone + '"></i>' +
                '           <i class="glyphicon glyphicon-remove-circle" data-toggle="tooltip" data-placement="top" title="Löschen" data-status="' + statDeleted + '"></i>' +
                '       </div>' +
                '   </div>' +
                '   <div class="panel-body">' + description + '</div>' +
                '</div>'
                );

            html.find('.panel-body').hide();
            makeClickable(html);
            html.appendTo(toDoList);

            if($('#todo-list').html().length < 0) {
                noTasksinQueue();
            } else {
                $('.ifNoJs').remove();
            }

            // activate tooltip
            $('[data-toggle="tooltip"]').tooltip();

        }
    });
}


function noTasksinQueue() {

    var html = $(
        '<div class="panel panel-danger task ifNoJs">' +
        '   <div class="panel-heading">' +
        '       <h3 class="panel-title">Sie haben aktuell keine Tasks</h3>' +
        '   </div>' +
        '   <div class="panel-body" style="display: none; height: 50px;">' +
        '       Klicken Sie auf den Button "Task erstellen' +
        '   </div>' +
        '</div>'
    );

    html.appendTo(toDoList);

}

function modals() {

    $('#createTask').on('click', function(e){
        var content = $(this).attr('href');
        e.preventDefault();
        $('#modal').modal('show');
        $('#modal').find('.modal-title').html('Erstelle eine Aufgabe');
        $('#modal').find('.modal-body').load(content);
        $('#modal').find('#modalConfirmBTN').html('Aufgabe Speichern');
        $('#modal').on('shown.bs.modal', function() {
          $(this).find('#taskTitle').focus();
        });
    });

    $('#test').on('click', function(e){
        var content = $(this).attr('href');
        e.preventDefault();
        $('#modal').modal('show');
        $('#modal').find('.modal-title').html('Test Modal Title');
        $('#modal').find('.modal-body').load(content);
        $('#modal').find('#modalConfirmBTN').html('OK').hide();
    });
}


function createTask() {

    $('#modalConfirmBTN').on('click', function() {

        var taskTitel = $('#taskTitle').val();
        var taskDescription = $('#taskDescription').val();
        var taskImportant = $("input[type='checkbox']").prop('checked');

        // if you click on the task safe btn do this
        if(this.innerHTML.indexOf('Aufgabe Speichern') !== -1) {

            if(taskTitel !== '' && taskDescription !== '') {

                var updateValue = 'taskTitel=' + taskTitel + '&taskDescription=' + taskDescription + '&important=' + taskImportant;

                $.ajax({
                    type: "post",
                    url: "src/php/addTask.php",
                    data: updateValue,
                    cache:false,
                    success: function() {
                        toDoList
                            .fadeOut(800, function () {
                                $(this)
                                    .empty()
                                    .add(sendRequest())
                                    .fadeIn()
                                ;
                            })
                        ;
                        $('#modal').modal('hide');
                    },
                    error: function() {
                        window.location.replace("404.html");
                    }
                });
            } else {

                var alertModal = '<div class="alert alert-danger" role="alert">Haben Sie alle felder ausgefüllt?</div>';
                if($('.alert').length) {
                    return false;
                } else {
                    $(alertModal).hide().appendTo('.modal-body').fadeIn(1000);
                }
            }
        } else {
            return false;
        }
    });
}



function editTask() {

    /*******
     * Set or unset the task important
     *******/

    toDoList.on('click', '.glyphicon-warning-sign', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&important=' + dataID +'&done=0&deleteTask=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .height(divheight)
                    .parent()
                    .addClass('loader');
                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                });
            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });

    /*******
     * Delete the task
     *******/

    toDoList.on('click', '.glyphicon-remove-circle', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&deleteTask=' + dataID +'&done=0&important=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .parent()
                    .addClass('loader');

                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .height(divheight)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                        noTasksinQueue();
                        setTimeout(function() {
                            toDoList.css('height','');
                        },10);
                    });

                deletedList.fadeOut(800, function () {
                    $(this)
                        .parent()
                        .removeClass('loader');
                    $(this)
                        .empty()
                        .add(deleteTask())
                        .fadeIn();
                });

            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });

    /*******
     * Recover the task
     *******/

    deletedList.on('click', '.glyphicon-repeat', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&deleteTask=' + dataID +'&done=0&important=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .height(divheight)
                    .parent()
                    .addClass('loader');
                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                        setTimeout(function() {
                            toDoList.css('height','');
                        },10);
                });
                deletedList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(deleteTask())
                            .fadeIn();
                        nothingDeleted();
                    });
            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });

    /*******
     * Mark a task as done or incomplete
     *******/

    toDoList.on('click', '.glyphicon-ok-circle', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&done=' + dataID +'&deleteTask=0&important=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .height(divheight)
                    .parent()
                    .addClass('loader');
                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                });
            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });
}




/*******
 * Set Task done
 *******/
function deleteTask() {
    $.get('src/php/deletedTasks.php', function (data) {
        for(var iteration = 0; iteration < data.length; iteration++) {
            var description = data[iteration].description.replace(/(\\n)+/g, '<br>');
            var taskHeadline = data[iteration].task;
            var taskID = data[iteration].id;
            var statDeleted = data[iteration].deleteTask;
            var panelColor = null;

            if(statDeleted == 1) {
                panelColor = 'success';
            } else {
                panelColor = 'default';
            }

            var html = $(
                '<div class="panel panel-'+ panelColor +' task">' +
                '   <div class="panel-heading">' +
                '       <h3 class="panel-title">' + taskHeadline + '</h3>' +
                '       <div id="' + taskID + '" class="icons pull-right">' +
                '           <i class="glyphicon glyphicon-repeat" data-toggle="tooltip" data-placement="top" title="Wiederherstellen" data-status="' + statDeleted + '"></i>' +
                '           <i id="deleteTaskFinal" class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Endgültig löschen" data-modal-content="src/html/modal/deleteTask.html"></i>' +
                '       </div>' +
                '   </div>' +
                '   <div class="panel-body">' + description + '</div>' +
                '</div>'
            );

            html.find('.panel-body').hide();
            makeClickable(html);
            html.appendTo(deletedList);

            if($('#deleted-tasks').html().length < 0) {
                nothingDeleted();
            } else {
                $('.ifDeletedTask').remove();
            }

            // activate tooltip
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
}


function nothingDeleted() {

    var html = $(
        '<div class="panel panel-success task ifDeletedTask">' +
            '<div class="panel-heading">' +
                '<h3 class="panel-title">Sie haben noch keine Tasks gelöscht</h3>' +
            '</div>' +
            '<div class="panel-body" style="display: none; height: 50px;">' +
                'Es liegt eine menge arbeit auf dem Tisch. Hau rein!!!' +
            '</div>' +
        '</div>'
    );

    html.appendTo(deletedList);

}

function deleteTaskFinal() {

    /*******
     * Delete the task final
     *******/

    $('body').on('click', '#deleteTaskFinal', function() {

        var taskID = $(this).parent().attr('id');
        var content = $(this).attr('data-modal-content');

        sessionStorage.setItem('taskID', taskID);

        $('#modal').modal('show');
        $('#modal').find('.modal-title').html('Aufgabe Löschen');
        $('#modal').find('.modal-body').load(content);
        $('#modal').find('#modalConfirmBTN').html('Endgültig Löschen');

        $('#modalConfirmBTN').on('click', function () {

            var updateValue = 'id=' + taskID + '&deleteFinal=1&done=0&important=0&deleteTask=0';

            $.ajax({
                type: "get",
                url: "src/php/editstate.php",
                data: updateValue,
                cache: false,
                success: function () {
                    deletedList
                        .fadeOut(800, function () {
                            $(this)
                                .parent()
                                .removeClass('loader');
                            $(this)
                                .empty()
                                .add(deleteTask())
                                .fadeIn();
                            nothingDeleted();
                            sessionStorage.clear();
                            $('#modal').modal('hide');
                        });
                },
                error: function () {
                    window.location.replace("404.html");
                }
            });
        });
    });
}
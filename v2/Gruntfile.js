module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // less to css
        less: {
            development: {
                options: {
                    paths: ['css']
                },
                files: {
                    'css/main.css': 'src/css/main.less'
                }
            },
            production: {
                options: {
                    paths: ['css'],
                    plugins: [
                        new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
                        new (require('less-plugin-clean-css'))({
                            advanced: true,
                            compatibility: 'ie8'
                        })
                    ],
                    modifyVars: {
                        imgPath: '"http://mycdn.com/path/to/images"',
                        bgColor: 'red'
                    }
                },
                files: {
                    'css/main-min.css': 'src/css/main.less'
                }
            }
        },

        // JS
        concat: {
            options: {
                separator: '\n\n',
            },
            dist: {
                src: ['src/js/*.js'],
                dest: 'js/main.js'
            }
        },
        jshint: {
            beforeconcat: ['src/js/*.js'],
            afterconcat: ['js/main.js']
        },

        //watcher
        watch: {
            scripts: {
                files: ['src/js/*.js', 'src/css/*.less'],
                tasks: ['jshint', 'concat', 'less'],
                options: {
                    spawn: false
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['less', 'jshint', 'concat', 'watch']);

};
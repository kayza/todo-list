<?php

require_once ('../../db.inc.php');

$todos = $connection->query('SELECT id, task, done, important, description, deleteTask FROM tblTasks WHERE deleteTask = 1 ORDER BY done ASC, important DESC;');

header('Content-Type: application/json');
echo json_encode($todos->fetch_all(MYSQLI_ASSOC));

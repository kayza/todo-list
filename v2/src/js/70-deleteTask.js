
/*******
 * Set Task done
 *******/
function deleteTask() {
    $.get('src/php/deletedTasks.php', function (data) {
        for(var iteration = 0; iteration < data.length; iteration++) {
            var description = data[iteration].description.replace(/(\\n)+/g, '<br>');
            var taskHeadline = data[iteration].task;
            var taskID = data[iteration].id;
            var statDeleted = data[iteration].deleteTask;
            var panelColor = null;

            if(statDeleted == 1) {
                panelColor = 'success';
            } else {
                panelColor = 'default';
            }

            var html = $(
                '<div class="panel panel-'+ panelColor +' task">' +
                '   <div class="panel-heading">' +
                '       <h3 class="panel-title">' + taskHeadline + '</h3>' +
                '       <div id="' + taskID + '" class="icons pull-right">' +
                '           <i class="glyphicon glyphicon-repeat" data-toggle="tooltip" data-placement="top" title="Wiederherstellen" data-status="' + statDeleted + '"></i>' +
                '           <i id="deleteTaskFinal" class="glyphicon glyphicon-trash" data-toggle="tooltip" data-placement="top" title="Endgültig löschen" data-modal-content="src/html/modal/deleteTask.html"></i>' +
                '       </div>' +
                '   </div>' +
                '   <div class="panel-body">' + description + '</div>' +
                '</div>'
            );

            html.find('.panel-body').hide();
            makeClickable(html);
            html.appendTo(deletedList);

            if($('#deleted-tasks').html().length < 0) {
                nothingDeleted();
            } else {
                $('.ifDeletedTask').remove();
            }

            // activate tooltip
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
}


function nothingDeleted() {

    var html = $(
        '<div class="panel panel-success task ifDeletedTask">' +
            '<div class="panel-heading">' +
                '<h3 class="panel-title">Sie haben noch keine Tasks gelöscht</h3>' +
            '</div>' +
            '<div class="panel-body" style="display: none; height: 50px;">' +
                'Es liegt eine menge arbeit auf dem Tisch. Hau rein!!!' +
            '</div>' +
        '</div>'
    );

    html.appendTo(deletedList);

}

function deleteTaskFinal() {

    /*******
     * Delete the task final
     *******/

    $('body').on('click', '#deleteTaskFinal', function() {

        var taskID = $(this).parent().attr('id');
        var content = $(this).attr('data-modal-content');

        sessionStorage.setItem('taskID', taskID);

        $('#modal').modal('show');
        $('#modal').find('.modal-title').html('Aufgabe Löschen');
        $('#modal').find('.modal-body').load(content);
        $('#modal').find('#modalConfirmBTN').html('Endgültig Löschen');

        $('#modalConfirmBTN').on('click', function () {

            var updateValue = 'id=' + taskID + '&deleteFinal=1&done=0&important=0&deleteTask=0';

            $.ajax({
                type: "get",
                url: "src/php/editstate.php",
                data: updateValue,
                cache: false,
                success: function () {
                    deletedList
                        .fadeOut(800, function () {
                            $(this)
                                .parent()
                                .removeClass('loader');
                            $(this)
                                .empty()
                                .add(deleteTask())
                                .fadeIn();
                            nothingDeleted();
                            sessionStorage.clear();
                            $('#modal').modal('hide');
                        });
                },
                error: function () {
                    window.location.replace("404.html");
                }
            });
        });
    });
}
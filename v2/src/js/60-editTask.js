function editTask() {

    /*******
     * Set or unset the task important
     *******/

    toDoList.on('click', '.glyphicon-warning-sign', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&important=' + dataID +'&done=0&deleteTask=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .height(divheight)
                    .parent()
                    .addClass('loader');
                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                });
            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });

    /*******
     * Delete the task
     *******/

    toDoList.on('click', '.glyphicon-remove-circle', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&deleteTask=' + dataID +'&done=0&important=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .parent()
                    .addClass('loader');

                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .height(divheight)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                        noTasksinQueue();
                        setTimeout(function() {
                            toDoList.css('height','');
                        },10);
                    });

                deletedList.fadeOut(800, function () {
                    $(this)
                        .parent()
                        .removeClass('loader');
                    $(this)
                        .empty()
                        .add(deleteTask())
                        .fadeIn();
                });

            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });

    /*******
     * Recover the task
     *******/

    deletedList.on('click', '.glyphicon-repeat', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&deleteTask=' + dataID +'&done=0&important=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .height(divheight)
                    .parent()
                    .addClass('loader');
                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                        setTimeout(function() {
                            toDoList.css('height','');
                        },10);
                });
                deletedList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(deleteTask())
                            .fadeIn();
                        nothingDeleted();
                    });
            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });

    /*******
     * Mark a task as done or incomplete
     *******/

    toDoList.on('click', '.glyphicon-ok-circle', function () {
        var taskValue = [0, 1];
        toggleAttr($(this), 'data-status', taskValue);

        var ID = $(this).parent().attr('id');
        var dataID = $(this).attr('data-status');
        var updateValue = 'id=' + ID + '&done=' + dataID +'&deleteTask=0&important=0&deleteFinal=0';
        var divheight = toDoList.height();

        $.ajax({
            type: "get",
            url: "src/php/editstate.php",
            data: updateValue,
            cache: false,
            success: function () {
                toDoList
                    .height(divheight)
                    .parent()
                    .addClass('loader');
                toDoList
                    .fadeOut(800, function () {
                        $(this)
                            .parent()
                            .removeClass('loader');
                        $(this)
                            .empty()
                            .add(sendRequest())
                            .fadeIn();
                });
            },
            error: function () {
                window.location.replace("404.html");
            }
        });
    });
}


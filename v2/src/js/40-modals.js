function modals() {

    $('#createTask').on('click', function(e){
        var content = $(this).attr('href');
        e.preventDefault();
        $('#modal').modal('show');
        $('#modal').find('.modal-title').html('Erstelle eine Aufgabe');
        $('#modal').find('.modal-body').load(content);
        $('#modal').find('#modalConfirmBTN').html('Aufgabe Speichern');
        $('#modal').on('shown.bs.modal', function() {
          $(this).find('#taskTitle').focus();
        });
    });

    $('#test').on('click', function(e){
        var content = $(this).attr('href');
        e.preventDefault();
        $('#modal').modal('show');
        $('#modal').find('.modal-title').html('Test Modal Title');
        $('#modal').find('.modal-body').load(content);
        $('#modal').find('#modalConfirmBTN').html('OK').hide();
    });
}


/*******
 * Set Task important or unimportant
 *******/
function sendRequest() {
    $.get('src/php/todolist.php', function (data) {
        for(var iteration = 0; iteration < data.length; iteration++) {
            var description = data[iteration].description.replace(/(\\n)+/g, '<br>');
            var taskHeadline = data[iteration].task;
            var taskID = data[iteration].id;
            var statImportant = data[iteration].important;
            var statDeleted = data[iteration].deleteTask;
            var statDone = data[iteration].done;
            var panelColor = null;

            if(statImportant == 1) {
                panelColor = 'danger';
            } else if(statDone == 1) {
                panelColor = 'info';
            } else if(statDeleted == 1) {
                panelColor = 'success';
            } else {
                panelColor = 'default';
            }

            var html = $(
                '<div class="panel panel-'+ panelColor +' task">' +
                '   <div class="panel-heading">' +
                '       <h3 class="panel-title">' + taskHeadline + '</h3>' +
                '       <div id="' + taskID + '" class="icons pull-right">' +
                '           <i class="glyphicon glyphicon-warning-sign" data-toggle="tooltip" data-placement="top" title="Als Wichtig markieren" data-status="' + statImportant + '"></i>' +
                '           <i class="glyphicon glyphicon-ok-circle" data-toggle="tooltip" data-placement="top" title="Als Erledigt markieren" data-status="' + statDone + '"></i>' +
                '           <i class="glyphicon glyphicon-remove-circle" data-toggle="tooltip" data-placement="top" title="Löschen" data-status="' + statDeleted + '"></i>' +
                '       </div>' +
                '   </div>' +
                '   <div class="panel-body">' + description + '</div>' +
                '</div>'
                );

            html.find('.panel-body').hide();
            makeClickable(html);
            html.appendTo(toDoList);

            if($('#todo-list').html().length < 0) {
                noTasksinQueue();
            } else {
                $('.ifNoJs').remove();
            }

            // activate tooltip
            $('[data-toggle="tooltip"]').tooltip();

        }
    });
}


function noTasksinQueue() {

    var html = $(
        '<div class="panel panel-danger task ifNoJs">' +
        '   <div class="panel-heading">' +
        '       <h3 class="panel-title">Sie haben aktuell keine Tasks</h3>' +
        '   </div>' +
        '   <div class="panel-body" style="display: none; height: 50px;">' +
        '       Klicken Sie auf den Button "Task erstellen' +
        '   </div>' +
        '</div>'
    );

    html.appendTo(toDoList);

}
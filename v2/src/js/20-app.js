var toDoList = jQuery('#todo-list');
var deletedList = jQuery('#deleted-tasks');

function initApp() {

    // if js active and/but no tasks in the queue
    if($('#todo-list').html().length < 0) {
        $('.ifNoJs').remove();
    } else {
        $('.ifNoJs').remove();
        noTasksinQueue();
    }

    // if no js
    $('#deleteTaskHeadline').removeClass('hide');
    $('.ifDeletedTask').removeClass('hide');

    // Name in footer
    $('footer').html('<p>&copy; kayzamedia 2016</p>');
}

function toggleAttr(el, attribute, vals){

    if ($(el).attr(attribute) ==  vals[0]){
        $(el).attr(attribute, vals[1]);

    } else if ($(el).attr(attribute) == vals[1]) {
        $(el).attr(attribute, vals[0]);
    }
}

function makeClickable(elem) {
    var that = elem.find('.panel-heading');

    that.on('click', function()
    {
        $('.panel-body').each(function () {
            if ($(this).closest('.panel').find('.panel-heading')[0] != that[0]) {
                $(this).slideUp('slow');
            }
        });

        elem
            .find('.panel-body')
            .slideDown('slow')
        ;
    });
}

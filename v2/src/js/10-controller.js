(function($) {
    $('document').ready(function () {
        initApp();
        sendRequest();
        modals();
        createTask();
        editTask();
        deleteTask();
        deleteTaskFinal();
    });
})(jQuery);

function createTask() {

    $('#modalConfirmBTN').on('click', function() {

        var taskTitel = $('#taskTitle').val();
        var taskDescription = $('#taskDescription').val();
        var taskImportant = $("input[type='checkbox']").prop('checked');

        // if you click on the task safe btn do this
        if(this.innerHTML.indexOf('Aufgabe Speichern') !== -1) {

            if(taskTitel !== '' && taskDescription !== '') {

                var updateValue = 'taskTitel=' + taskTitel + '&taskDescription=' + taskDescription + '&important=' + taskImportant;

                $.ajax({
                    type: "post",
                    url: "src/php/addTask.php",
                    data: updateValue,
                    cache:false,
                    success: function() {
                        toDoList
                            .fadeOut(800, function () {
                                $(this)
                                    .empty()
                                    .add(sendRequest())
                                    .fadeIn()
                                ;
                            })
                        ;
                        $('#modal').modal('hide');
                    },
                    error: function() {
                        window.location.replace("404.html");
                    }
                });
            } else {

                var alertModal = '<div class="alert alert-danger" role="alert">Haben Sie alle felder ausgefüllt?</div>';
                if($('.alert').length) {
                    return false;
                } else {
                    $(alertModal).hide().appendTo('.modal-body').fadeIn(1000);
                }
            }
        } else {
            return false;
        }
    });
}


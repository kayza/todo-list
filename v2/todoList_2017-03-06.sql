# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.7.13)
# Datenbank: todoList
# Erstellt am: 2017-03-05 23:14:00 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Export von Tabelle tblTasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblTasks`;

CREATE TABLE `tblTasks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `task` char(50) DEFAULT NULL,
  `done` bit(1) DEFAULT NULL,
  `important` bit(1) DEFAULT NULL,
  `description` text,
  `deleteTask` bit(1) DEFAULT NULL,
  `deleteFinal` bit(1) DEFAULT NULL,
  `usrID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblTasks` WRITE;
/*!40000 ALTER TABLE `tblTasks` DISABLE KEYS */;

INSERT INTO `tblTasks` (`id`, `task`, `done`, `important`, `description`, `deleteTask`, `deleteFinal`, `usrID`)
VALUES
	(1,'Lorem ipsum dolor sit amet',b'0',b'0','Lorem ipsum dolor sit amet, consectetur adipiscing elit. \\n\\nMorbi sit amet tortor metus. Praesent rutrum sapien sit amet nisi dignissim suscipit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec sit amet porta diam. Pellentesque in fi...',b'0',b'0',1),
	(2,'Cras porta elit et est',b'0',b'0','Donec tristique enim arcu, eget consectetur urna tempor sit amet. \\nAenean mollis, urna sit amet tincidunt imperdiet, velit elit pellentesque sapien, facilisis lacinia est magna sed leo. \\nFusce ligula ipsum, egestas sit amet consectetur eleifend, imperdiet quis sapien. \\nPellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. \\nPraesent vel ullamcorper erat, vitae molestie nunc.\n',b'0',b'0',1),
	(3,'Fusce scelerisque sem',b'0',b'0','<section>Ut quis lorem a dui venenatis cursus.</section> <p>Aenean a nibh fringilla, posuere mauris vel, bibendum ligula. Aenean vitae orci id tellus commodo accumsan. Sed sagittis maximus semper. Pellentesque sit amet tristique eros, sed porta ante. Don...',b'0',b'0',1),
	(4,'Nulla nec tortor dapibus nibh',b'0',b'0','Sed et ipsum bibendum, feugiat ex id, convallis sapien. Mauris elementum lectus eget turpis hendrerit imperdiet. Integer venenatis massa a ipsum tincidunt, eu euismod neque elementum. Vivamus venenatis nunc a eros suscipit, quis mattis lectus blandit. Morbi ut pulvinar massa.',b'0',b'0',1),
	(5,'Phasellus fermentum',b'0',b'0','Vestibulum leo arcu, dignissim a imperdiet a, iaculis nec libero. Maecenas tincidunt, nulla ac tincidunt fermentum, urna urna volutpat urna, elementum eleifend odio est sit amet ante. Sed in fringilla augue, at sodales turpis. Praesent eget imperdiet odio. Praesent euismod ante lectus, nec faucibus urna tincidunt eget. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae.',b'0',b'1',1),
	(6,'Duis at orci id est placerat auctor',b'0',b'0','Etiam viverra odio metus, vitae maximus quam auctor vel. In non auctor enim. Aliquam sed turpis a sem congue facilisis. Sed feugiat nisi et diam sagittis, sit amet convallis nulla pellentesque. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.',b'0',b'0',1),
	(7,'Donec vel justo',b'0',b'0','Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed bibendum eu velit laoreet luctus. Etiam et elementum ex, at molestie ante. Morbi sed tortor ligula. Nulla lectus sem, laoreet eget urna ornare, imperdiet aliquet lectus. Aliquam pharetra sollicitudin tortor, sit amet porta dui gravida eu. Quisque consectetur erat enim, id rutrum nulla blandit sed.',b'0',b'0',1),
	(8,'taskk',b'0',b'0','frr\n',b'0',b'1',1);

/*!40000 ALTER TABLE `tblTasks` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblUser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblUser`;

CREATE TABLE `tblUser` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL DEFAULT '',
  `passwort` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tblUser` WRITE;
/*!40000 ALTER TABLE `tblUser` DISABLE KEYS */;

INSERT INTO `tblUser` (`id`, `email`, `passwort`)
VALUES
	(1,'test@mail.com','098f6bcd4621d373cade4e832627b4f6');

/*!40000 ALTER TABLE `tblUser` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
